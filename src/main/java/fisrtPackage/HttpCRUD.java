package fisrtPackage;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class HttpCRUD {
    private HttpContext context;
    private HttpServer server;

    public void listen(int port) throws IOException {
        server = HttpServer.create(new InetSocketAddress(port), 0);

        this.context = server.createContext("/");
        this.context.setHandler(HttpCRUD::handleRequest);

        server.start();
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {
        URI uri = exchange.getRequestURI();
        String method = exchange.getRequestMethod();

        System.out.println(uri);
        System.out.println("method: " + method);


        if (method.equals("GET")) {
            System.out.println("GET request");

        } else if (method.equals("POST")) {
            System.out.println("POST request");

        } else if (method.equals("DELETE")) {
            System.out.println("DELETE request");

        } else if (method.equals("PUT")) {
            System.out.println("PUT request");

        } else {
            System.out.println("Not allowed request method: '" + method + "'");
        }

        String response = "Hi there!";
        exchange.sendResponseHeaders(200, response.getBytes().length);

        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    private void callRoute(List<Router.Route> routes, URI uri, HttpExchange exchange) {
        routes.forEach(route -> {
            if (route.uri.equals(uri)) {
//                route.callback();
            }
        });
    }

    public static class Router {
        public static List<Route> getRoutes = new ArrayList<Route>();
        public static List<Route> postRoutes = new ArrayList<Route>();
        public static List<Route> deleteRoutes = new ArrayList<Route>();
        public static List<Route> putRoutes = new ArrayList<Route>();


        public static void get(String uri, Method callback) {
            getRoutes.add(new Route(uri, callback, "GET"));
        }

        public static void post(String uri, Method callback) {
            postRoutes.add(new Route(uri, callback, "POST"));
        }

        public static void delete(String uri, Method callback) {
            deleteRoutes.add(new Route(uri, callback, "DELETE"));
        }

        public static void put(String uri, Method callback) {
            putRoutes.add(new Route(uri, callback, "PUT"));
        }

        private static class Route {
            String uri;
            Method callback;
            String method;

            Route(String uri, Method callback, String method) {
                this.uri =  uri;
                this.callback =  callback;
                this.method =  method;
            }
        }
    }
}
